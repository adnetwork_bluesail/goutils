package goutils

import (
	"encoding/json"
	"errors"
	"time"

	asc "github.com/aerospike/aerospike-client-go"
)

// AeroSpike struct
type AeroSpike struct {
	Client      *asc.Client
	Namespace   string
	Policy      *asc.BasePolicy
	WritePolicy *asc.WritePolicy
}

// GetAeroSpike func
func GetAeroSpike(host string, port int, ns string) (*AeroSpike, error) {
	client, err := asc.NewClient(host, port)
	if err != nil {
		return nil, err
	}
	return &AeroSpike{
		Client: client, Namespace: ns, Policy: asc.NewPolicy(), WritePolicy: asc.NewWritePolicy(0, 0),
	}, nil
}

func (aes *AeroSpike) getKey(set string, key string) *asc.Key {
	k, err := asc.NewKey(aes.Namespace, set, key)
	if err != nil {
		return nil
	}
	return k
}

// Cache func
func (aes *AeroSpike) Cache(set string, key string, binName string, value string, exp uint32) error {
	bin := asc.BinMap{binName: value}
	writePolicy := aes.WritePolicy
	writePolicy.Expiration = exp
	return aes.Client.Put(writePolicy, aes.getKey(set, key), bin)
}

// Get func
func (aes *AeroSpike) Get(set string, _key string) (map[string]interface{}, error) {
	key, err := asc.NewKey(aes.Namespace, set, _key)
	if err != nil {
		return nil, err
	}
	record, err := aes.Client.Get(aes.Policy, key)
	if err != nil || record == nil {
		return nil, err
	}
	return record.Bins, nil
}

// GetObject func
func (aes *AeroSpike) GetObject(set string, _key string, obj interface{}) error {
	key, err := asc.NewKey(aes.Namespace, set, _key)
	if err != nil {
		return err
	}
	err = aes.Client.GetObject(aes.Policy, key, obj)
	return err
}

// SetObject func
func (aes *AeroSpike) SetObject(set string, _key string, obj interface{}) error {
	key, err := asc.NewKey(aes.Namespace, set, _key)
	if err != nil {
		return err
	}
	err = aes.Client.PutObject(aes.WritePolicy, key, obj)
	return err
}

// SetObjWithTimeParam func
func (aes *AeroSpike) SetObjWithTimeParam(set string, _key string, bin string, obj interface{}, timestamp int64, exp uint32) error {
	j, err := json.Marshal(obj)
	if err != nil {
		return err
	}
	bins := make(map[string]interface{})
	bins["timestamps"] = timestamp
	bins[bin] = string(j)
	key, err := asc.NewKey(aes.Namespace, set, _key)
	if err != nil {
		return err
	}
	writePolicy := aes.WritePolicy
	if exp > 0 {
		writePolicy.Expiration = exp
	}
	err = aes.Client.Put(writePolicy, key, bins)
	return err
}

// SetObjWithTime ...
func (aes *AeroSpike) SetObjWithTime(set string, _key string, bin string, obj interface{}, exp uint32) error {
	curr := time.Now().UTC().Unix()
	return aes.SetObjWithTimeParam(set, _key, bin, obj, curr, exp)
}

// GetWithObj ...
func (aes *AeroSpike) GetWithObj(set string, _key string, binName string, obj interface{}) error {
	bins, err := aes.Get(set, _key)
	if err != nil {
		return err
	}
	bin := bins[binName]
	if bin == nil {
		return errors.New("Not found")
	}
	err = json.Unmarshal([]byte(bin.(string)), obj)
	return err
}

// SetBin ...
func (aes *AeroSpike) SetBin(set string, _key string, bindName string, obj interface{}) error {
	key, err := asc.NewKey(aes.Namespace, set, _key)
	if err != nil {
		return err
	}
	bin := asc.NewBin(bindName, obj)
	err = aes.Client.PutBins(aes.WritePolicy, key, bin)
	return err
}

// SetBinString ...
func (aes *AeroSpike) SetBinString(set string, key string, binName string, obj interface{}) error {
	j, err := json.Marshal(obj)
	if err != nil {
		return nil
	}
	return aes.SetBin(set, key, binName, string(j))
}

// Del func
func (aes *AeroSpike) Del(set string, key string) (bool, error) {
	return aes.DelByKey(aes.getKey(set, key))
}

// DelByKey func
func (aes *AeroSpike) DelByKey(key *asc.Key) (bool, error) {
	return aes.Client.Delete(aes.WritePolicy, key)
}

// QueryRange func
func (aes *AeroSpike) QueryRange(set string, bin string, min int64, max int64) (*asc.Recordset, error) {
	stmt := asc.NewStatement(aes.Namespace, set, "value", "timestamps")
	stmt.Addfilter(asc.NewRangeFilter(bin, min, max))
	return aes.Client.Query(nil, stmt)
}

// DelRange func
// function to delete records by range
func (aes *AeroSpike) DelRange(set string, bin string, min int64, max int64) (*asc.ExecuteTask, error) {
	stmt := asc.NewStatement(aes.Namespace, set, bin)
	stmt.Addfilter(asc.NewRangeFilter(bin, min, max))
	return aes.Client.ExecuteUDF(nil, stmt, "delete", "deleteRecord")
}
