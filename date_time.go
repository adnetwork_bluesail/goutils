package goutils

import (
	"strconv"
	"time"
)

// GetCurrDateTimeLabel func
func GetCurrDateTimeLabel() string {
	current := time.Now()
	return GetDateTimeLabel(current)
}

// GetDateTimeLabel func
func GetDateTimeLabel(dateTime time.Time) string {
	key := dateTime.Format("2006-1-2") + "-" + strconv.Itoa(dateTime.Hour())
	minute := dateTime.Minute()
	quotient := (minute / 5) * 5
	if quotient < 10 {
		key += "-0" + strconv.Itoa(quotient)
	} else {
		key += "-" + strconv.Itoa(quotient)
	}
	return key
}

// GetDateTimeLableUnix func
func GetDateTimeLableUnix(unix int64) string {
	tm := time.Unix(unix, 0)
	return GetDateTimeLabel(tm)
}

// GetDateHourLabel func
func GetDateHourLabel(unix int64) string {
	tm := time.Unix(unix, 0)
	return tm.Format("2006-1-2") + "-" + strconv.Itoa(tm.Hour())
}
