package goutils

import "github.com/avct/uasurfer"

// DeviceType
const (
	MobileOrTabletType  = 1
	PCType              = 2
	ConnectedTVType     = 3
	MobileType          = 4
	TabletType          = 5
	ConnectedDeviceType = 6
	SetTopBoxType       = 7
)

// GetDeviceType function
func GetDeviceType(uaString string) int8 {
	uaSurf := uasurfer.Parse(uaString)
	if uaSurf.DeviceType == uasurfer.DevicePhone {
		return MobileType
	}
	if uaSurf.DeviceType == uasurfer.DeviceTablet {
		return TabletType
	}
	if uaSurf.DeviceType == uasurfer.DeviceTV {
		return ConnectedTVType
	}
	return PCType
}

// GetDeviceLang function
func GetDeviceLang(acceptLang string) string {
	s := []rune(acceptLang)
	return string(s[0:2])
}
