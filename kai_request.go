package goutils

import (
	"net/url"
)

// KaiRequest struct
type KaiRequest struct {
	apiPath string
	req     *Request
}

// GetKaiReq func
func GetKaiReq(apiPath string, secret string, beacon string, super string) *KaiRequest {
	token := &Token{secretKey: secret, super: super}
	superToken, _ := token.SignSuperToken()
	headers := map[string]string{
		"content-type":  "application/json",
		"authorization": beacon + superToken,
	}
	return &KaiRequest{
		apiPath: apiPath, req: &Request{Headers: headers},
	}
}

func (k *KaiRequest) getURL(path string) string {
	_, err := url.ParseRequestURI(path)
	if err != nil {
		return k.apiPath + path
	}
	return path
}

// Get func
func (k *KaiRequest) Get(path string) (string, error) {
	return k.req.Get(k.getURL(path))
}

// Post func
func (k *KaiRequest) Post(path string, reqBody interface{}) (string, error) {
	return k.req.Post(k.getURL(path), reqBody)
}

// Patch func
func (k *KaiRequest) Patch(path string, reqBody interface{}) (string, error) {
	return k.req.Patch(k.getURL(path), reqBody)
}
