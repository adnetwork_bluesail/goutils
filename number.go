package goutils

import (
	"math/rand"
	"time"
)

// RandNumberInRange func
func RandNumberInRange(min, max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return rand.Intn(max-min) + min
}
