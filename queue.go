package goutils

import (
	"github.com/streadway/amqp"
)

// Queue struct
type Queue struct {
	Conn *amqp.Connection
}

// Func to process in queue
type Func func(msg string) bool

// GetQueue func
func GetQueue(user string, pass string, host string, port string) Queue {
	return Queue{
		Conn: Connect(user, pass, host, port),
	}
}

// Connect func
func Connect(user string, pass string, host string, port string) *amqp.Connection {
	conn, err := amqp.Dial("amqp://" + user + ":" + pass + "@" + host + ":" + port)
	if err != nil {
		panic(err)
	}
	return conn
}

// Consumer func
func (q *Queue) Consumer(queueName string, fn Func) {
	channel, err := q.Conn.Channel()
	logOnError(err)
	defer channel.Close()
	queue, err := channel.QueueDeclare(queueName, true, false, false, false, nil)
	logOnError(err)
	err = channel.Qos(
		1,     // prefetch count
		0,     // prefetch size
		false, // global
	)
	logOnError(err)
	msgs, err := channel.Consume(
		queue.Name, // queue
		"",         // consumer
		false,      // auto-ack
		false,      // exclusive
		false,      // no-local
		false,      // no-wait
		nil,        // args
	)
	logOnError(err)
	forever := make(chan bool)
	go func() {
		for d := range msgs {
			if fn(string(d.Body)) == true {
				d.Ack(false)
			} else {
				d.Nack(false, true)
			}
		}
	}()
	<-forever
}

// Publish func
func (q *Queue) Publish(queueName, msg string) {
	ch, err := q.Conn.Channel()
	logOnError(err)
	defer ch.Close()
	err = ch.Publish(
		"",        // exchange
		queueName, // routing key
		false,     // mandatory
		false,     // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(msg),
		})
	logOnError(err)
}

func logOnError(err error) {
	if err != nil {
		panic(err)
	}

}
