package goutils

import (
	"encoding/json"
	"time"

	"github.com/go-redis/redis"
)

// Redis struct
type Redis struct {
	prefix string
	client *redis.Client
}

// Connect func
func (r *Redis) Connect(host string, port string, auth string, dbIndex int, prefix string) {
	r.prefix = prefix
	session := redis.NewClient(&redis.Options{
		Addr: host + ":" + port, Password: auth, DB: dbIndex,
	})
	r.client = session
}

func (r *Redis) getFullHash(_hash string) string {
	return r.prefix + _hash
}

// SetCache func
func (r *Redis) SetCache(hash string, value string, duration int) {
	_, _ = r.client.SetNX(r.getFullHash(hash), value, time.Duration(duration)*time.Minute).Result()
}

// Get func
func (r *Redis) Get(_hash string) (string, error) {
	return r.client.Get(r.getFullHash(_hash)).Result()
}

// HSet ...
func (r *Redis) HSet(_hash string, field string, value string) {
	r.client.HSet(r.getFullHash(_hash), field, value)
}

// HGet ...
func (r *Redis) HGet(_hash string, feild string) (string, error) {
	return r.client.HGet(r.getFullHash(_hash), feild).Result()

}

// SAdd function
func (r *Redis) SAdd(_hash string, member string) {
	r.client.SAdd(r.getFullHash(_hash), member)
}

// HGetAll ...
func (r *Redis) HGetAll(_hash string) (map[string]string, error) {
	return r.client.HGetAll(r.getFullHash(_hash)).Result()
}

// HDel ...
func (r *Redis) HDel(_hash string, field string) {
	r.client.HDel(r.getFullHash(_hash), field)
}

// HSetStruct ...
func (r *Redis) HSetStruct(hash string, field string, obj interface{}) error {
	objJSON, err := json.Marshal(obj)
	if err != nil {
		return err
	}
	r.HSet(hash, field, string(objJSON))
	return nil
}

// SCard ...
func (r *Redis) SCard(hash string) (int64, error) {
	return r.client.SCard(r.getFullHash(hash)).Result()
}
