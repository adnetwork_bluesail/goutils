package goutils

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// Request struct
type Request struct {
	Headers map[string]string
	TimeOut time.Duration
}

// Do function
func (r *Request) Do(method string, url string, reqBody interface{}) (string, error) {
	httpClient := &http.Client{}
	if r.TimeOut > 0 {
		httpClient.Timeout = time.Duration(r.TimeOut * time.Millisecond)
	}
	req, _ := http.NewRequest(strings.ToUpper(method), url, getReqBody(reqBody))
	if len(r.Headers) > 0 {
		r.setHeader(req)
	}
	resp, reqErr := httpClient.Do(req)
	if reqErr != nil {
		return "", reqErr
	}
	if !(strings.HasPrefix(strconv.Itoa(resp.StatusCode), "2")) {
		return "", errors.New("Internal Service Error")
	}
	defer resp.Body.Close()
	body, parseErr := ioutil.ReadAll(resp.Body)
	return string(body), parseErr
}

func getReqBody(reqBody interface{}) io.Reader {
	if reqBody == nil {
		return nil
	}
	jsonBody, err := json.Marshal(reqBody)
	if err != nil {
		return nil
	}
	return bytes.NewBuffer(jsonBody)
}

// Get funtion
func (r *Request) Get(url string) (string, error) {
	return r.Do("Get", url, nil)
}

func (r *Request) setHeader(req *http.Request) *http.Request {
	for index, elem := range r.Headers {
		req.Header.Add(index, elem)
	}
	return req
}

// Post function
func (r *Request) Post(url string, reqBody interface{}) (string, error) {
	return r.Do("POST", url, reqBody)
}

// Patch func
func (r *Request) Patch(url string, reqBody interface{}) (string, error) {
	return r.Do("PATCH", url, reqBody)
}
