package goutils

import (
	"testing"

	"github.com/stretchr/testify/require"

	"github.com/stretchr/testify/assert"
)

func TestReverseString(t *testing.T) {
	str := "Hello"
	actualResult := ReverseString(str)
	expectedResult := "olleH"
	assert.Equal(t, expectedResult, actualResult)
	require.Equal(t, expectedResult, actualResult)
}
