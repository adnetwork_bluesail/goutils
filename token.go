package goutils

import (
	jwt "github.com/dgrijalva/jwt-go"
)

var token *Token

// InitToken func
func InitToken(secretKey string, super string) {
	token = &Token{
		secretKey: secretKey,
		super:     super,
	}
}

// GetToken func
func GetToken(secretKey string, super string) *Token {
	if token == nil {
		InitToken(secretKey, super)
	}
	return token
}

// VerifyToken func
func VerifyToken(secret string, encodedToken string) (*jwt.Token, error) {
	token, err := jwt.Parse(encodedToken, func(token *jwt.Token) (interface{}, error) {
		return []byte(secret), nil
	})
	if err != nil {
		return &jwt.Token{}, err
	}
	return token, nil
}

// Token struct
type Token struct {
	secretKey string
	super     string
}

// SignSuperToken func
func (t *Token) SignSuperToken() (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"userId": t.super,
	})
	return token.SignedString([]byte(t.secretKey))
}

// VerifyToken func
func (t *Token) VerifyToken(encodedToken string) (*jwt.Token, error) {
	return VerifyToken(string(t.secretKey), encodedToken)
}

// SignToken func
func (t *Token) SignToken(mapClaims *jwt.MapClaims) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, mapClaims)
	tokenStr, _ := token.SignedString([]byte(t.secretKey))
	return tokenStr
}

// ParseWithClaims func
func (t *Token) ParseWithClaims(encodedToken string, claims jwt.Claims) (*jwt.Token, error) {
	token, err := jwt.ParseWithClaims(encodedToken, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(t.secretKey), nil
	})
	return token, err
}
